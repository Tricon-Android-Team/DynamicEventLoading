package com.example.ankit.dynamiceventloading.dataModels;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("_id")
    public String userId;

    @SerializedName("full_name")
    public String fullName;

    public String email;

    public Image image;

}
