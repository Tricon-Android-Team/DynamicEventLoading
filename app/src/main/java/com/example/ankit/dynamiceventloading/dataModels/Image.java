package com.example.ankit.dynamiceventloading.dataModels;

import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("normal")
    public String normalUrl;

    @SerializedName("thumb")
    public String thumbUrl;

}
