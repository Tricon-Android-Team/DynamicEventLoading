package com.example.ankit.dynamiceventloading.dataModels;

import com.google.gson.annotations.SerializedName;

public class Group {

    @SerializedName("_id")
    public String groupId;

    public String name;

    @SerializedName("picture_url")
    public String pictureUrl;
}
