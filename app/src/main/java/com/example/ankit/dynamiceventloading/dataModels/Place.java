package com.example.ankit.dynamiceventloading.dataModels;

import com.google.gson.annotations.SerializedName;

public class Place {

    @SerializedName("_id")
    public String placeId;

    @SerializedName("loc")
    public double[] location;

    @SerializedName("name")
    public String placeName;

    @SerializedName("room_id")
    public String roomId;

    @SerializedName("room_name")
    public String roomName;

    public String address;

}
