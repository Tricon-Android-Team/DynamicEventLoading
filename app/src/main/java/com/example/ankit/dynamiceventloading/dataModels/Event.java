package com.example.ankit.dynamiceventloading.dataModels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Event {

    @SerializedName("_id")
    public String eventId;

    public String description;

    @SerializedName("name")
    public String eventName;

    @SerializedName("start_time")
    public String startTime;

    @SerializedName("end_time")
    public String endTime;

    @SerializedName("attending_status")
    public String attendingStatus;

    @SerializedName("top_attending_users")
    public List<User> topAttendingUsers;

    @SerializedName("list")
    public Group group;

    @SerializedName("image")
    public Image eventImage;

    @SerializedName("invitor_names")
    public String invitedBy;

    public Place place;

    @SerializedName("time_zone")
    public String timeZone;

    public static final String ATTENDING_STATUS_YES = "y";
    public static final String ATTENDING_STATUS_NO = "n";
    public static final String ATTENDING_STATUS_MAY_BE = "-";

}
