package com.example.ankit.dynamiceventloading.asyncTasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ImageLoadingTask extends AsyncTask<String, Void, Bitmap> {

    private WeakReference<ImageView> mImageViewWeakReference;

    public ImageLoadingTask(ImageView imageView) {
        mImageViewWeakReference = new WeakReference<>(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... params) {

        if (params != null) {
            String url = params[0];

            if (!TextUtils.isEmpty(url)) {
                URL webUrl = null;
                try {
                    webUrl = new URL(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                if (webUrl != null) {
                    try {
                        HttpURLConnection connection = (HttpURLConnection) webUrl.openConnection();
                        InputStream is = connection.getInputStream();
                        return BitmapFactory.decodeStream(is);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (bitmap != null) {
            ImageView imageView = mImageViewWeakReference.get();

            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }

            mImageViewWeakReference.clear();
        }
    }
}
