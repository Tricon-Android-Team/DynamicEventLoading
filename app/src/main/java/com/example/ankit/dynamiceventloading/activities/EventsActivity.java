package com.example.ankit.dynamiceventloading.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ankit.dynamiceventloading.R;
import com.example.ankit.dynamiceventloading.asyncTasks.ImageLoadingTask;
import com.example.ankit.dynamiceventloading.custom.GravityCompoundDrawable;
import com.example.ankit.dynamiceventloading.dataModels.Event;
import com.example.ankit.dynamiceventloading.dataModels.Place;
import com.example.ankit.dynamiceventloading.dataModels.User;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.GsonBuilder;

import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EventsActivity extends AppCompatActivity {

    private Event mEvent;

    private static final int REQUEST_INTERNET_PERMISSION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        //decode event resource file
        decodeEventResourceFile();

        //setup toolbar
        setupToolbar();

        //setup fab
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }

        //check permissions
        checkPermissions();

        //setup content views
        setupContentView();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_INTERNET_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, R.string.msg_internet_permission, Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                break;
        }
    }

    private void decodeEventResourceFile() {
        InputStreamReader reader = new InputStreamReader(getResources().openRawResource(R.raw.event));
        mEvent = new GsonBuilder().create().fromJson(reader, Event.class);
    }

    private void setupToolbar() {
        //set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set actionbar title empty
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
        }

        //set collapsing toolbar title animation
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        if (collapsingToolbarLayout != null && appBarLayout != null) {
            appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                boolean isShow = false;
                int scrollRange = -1;

                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.getTotalScrollRange();
                    }
                    if (scrollRange + verticalOffset == 0) {
                        collapsingToolbarLayout.setTitle(mEvent.eventName);
                        isShow = true;
                    } else if (isShow) {
                        collapsingToolbarLayout.setTitle("");
                        isShow = false;
                    }
                }
            });
        }
    }

    private void checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)) {
                CoordinatorLayout eventContainer = (CoordinatorLayout) findViewById(R.id.event_container);
                if (eventContainer != null) {
                    Snackbar.make(eventContainer, R.string.msg_internet_permission, Snackbar.LENGTH_INDEFINITE)
                            .setAction("Allow", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ActivityCompat.requestPermissions(EventsActivity.this,
                                            new String[]{Manifest.permission.INTERNET},
                                            REQUEST_INTERNET_PERMISSION);
                                }
                            })
                            .show();
                }
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.INTERNET},
                        REQUEST_INTERNET_PERMISSION);
            }
        }
    }

    private void setupContentView() {

        //set header image
        setHeaderImage();

        //set event name
        setEventName();

        //set event time
        setEventTime();

        //set attending status
        setAttendingStatus();

        //set address
        setAddress();

        //set event description
        setEventDescription();

        //set attendees
        setAttendees();

        //set attached group
        setAttachedGroup();

        //set invited by
        setInvitedBy();
    }

    private void setHeaderImage() {
        //set header image
        if (mEvent.eventImage != null && !TextUtils.isEmpty(mEvent.eventImage.normalUrl)) {

            //get header view stub
            ViewStub vsHeader = (ViewStub) findViewById(R.id.vs_header);
            if (vsHeader != null) {

                //inflate header view stub
                ImageView ivHeader = (ImageView) vsHeader.inflate();
                if (ivHeader != null) {
                    loadImage(ivHeader, mEvent.eventImage.normalUrl);
                }
            }
        }
    }

    private void setEventName() {
        //set event name
        TextView tvEventName = (TextView) findViewById(R.id.tv_event_name);
        if (tvEventName != null) {
            tvEventName.setText(mEvent.eventName);
        }
    }

    private void setEventTime() {
        //set start time
        TextView tvStartTime = (TextView) findViewById(R.id.tv_start_time);
        if (tvStartTime != null) {
            try {
                //parse date to required date format
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd'T'hh:mm:ss'Z'", Locale.getDefault());
                Date date = simpleDateFormat.parse(mEvent.startTime);

                SimpleDateFormat requiredDateFormat = new SimpleDateFormat("EEEE, MMM dd\nh:mm a", Locale.getDefault());

                tvStartTime.setText(requiredDateFormat.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
                tvStartTime.setText(mEvent.startTime);
            }
        }

        //set end time
        TextView tvEndTime = (TextView) findViewById(R.id.tv_end_time);
        if (tvEndTime != null) {
            try {
                //parse date to required date format
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy-MM-dd'T'hh:mm:ss'+'hh:mm", Locale.getDefault());
                Date date = simpleDateFormat.parse(mEvent.endTime);

                SimpleDateFormat requiredDateFormat = new SimpleDateFormat("EEEE, MMM dd\nh:mm a", Locale.getDefault());

                tvEndTime.setText(requiredDateFormat.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
                tvEndTime.setText(mEvent.endTime);
            }
        }
    }

    private void setAttendingStatus() {
        //set attending status
        switch (mEvent.attendingStatus) {

            case Event.ATTENDING_STATUS_YES:
                Button btnYes = (Button) findViewById(R.id.btn_yes);
                if (btnYes != null) {
                    btnYes.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                break;

            case Event.ATTENDING_STATUS_NO:
                Button btnNo = (Button) findViewById(R.id.btn_no);
                if (btnNo != null) {
                    btnNo.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                break;

            case Event.ATTENDING_STATUS_MAY_BE:
                Button btnMayBe = (Button) findViewById(R.id.btn_may_be);
                if (btnMayBe != null) {
                    btnMayBe.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                break;

            default:
                break;
        }
    }

    private void setAddress() {
        if (mEvent.place != null) {
            final Place place = mEvent.place;

            //get address view stub
            ViewStub vsAddress = (ViewStub) findViewById(R.id.vs_address);
            if (vsAddress != null) {

                //inflate address view stub
                LinearLayout llAddressView = (LinearLayout) vsAddress.inflate();
                if (llAddressView != null) {

                    //showing address, map and gt directions options only when address name is not null
                    if (!TextUtils.isEmpty(place.placeName)) {

                        //set address name
                        TextView tvAddress = (TextView) llAddressView.findViewById(R.id.tv_address);
                        if (tvAddress != null) {
                            String address = place.placeName;
                            if (!TextUtils.isEmpty(place.address)) {
                                address += "\n" + place.address;
                            }
                            tvAddress.setText(address);
                        }

                        //if location is not null then inflate "map" and "get directions option" otherwise not
                        if (place.location != null) {
                            LayoutInflater.from(this)
                                    .inflate(R.layout.content_event_directions, llAddressView, true);

                            //load map fragment
                            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                    .findFragmentById(R.id.map);

                            //load map asynchronously
                            mapFragment.getMapAsync(new OnMapReadyCallback() {
                                @Override
                                public void onMapReady(GoogleMap googleMap) {
                                    // Add a marker in event location and move the camera.
                                    LatLng eventLocation = new LatLng(place.location[0], place.location[1]);
                                    googleMap.addMarker(new MarkerOptions()
                                            .position(eventLocation)
                                            .title(place.placeName));
                                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(eventLocation));
                                }
                            });
                        }
                    }
                }
            }

        }
    }

    private void setEventDescription() {
        //set event description
        if (!TextUtils.isEmpty(mEvent.description)) {

            //get event description view stub
            ViewStub vsDescription = (ViewStub) findViewById(R.id.vs_description);
            if (vsDescription != null) {

                //inflate event description view stub
                TextView tvDescription = (TextView) vsDescription.inflate();
                if (tvDescription != null) {

                    //set description text
                    tvDescription.setText(mEvent.description);

                    //set compound drawable to description text view
                    float drawablePadding = getResources().getDimension(R.dimen.drawable_padding);
                    Drawable innerDrawable = getResources().getDrawable(R.drawable.ic_description_gray_24dp);
                    if(innerDrawable != null) {
                        GravityCompoundDrawable gravityDrawable = new GravityCompoundDrawable(innerDrawable, drawablePadding);
                        innerDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
                        gravityDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
                        tvDescription.setCompoundDrawables(gravityDrawable, null, null, null);
                    }
                }
            }
        }
    }

    private void setAttendees() {
        //set attendees
        if (mEvent.topAttendingUsers != null && mEvent.topAttendingUsers.size() > 0) {
            //get attendees view stub
            ViewStub vsAttendees = (ViewStub) findViewById(R.id.vs_attendees);
            if (vsAttendees != null) {

                //inflate attendees view stub
                LinearLayout llAttendees = (LinearLayout) vsAttendees.inflate();
                if (llAttendees != null) {

                    //get people's view
                    final LinearLayout llPeople = (LinearLayout) llAttendees.findViewById(R.id.ll_people);
                    if (llPeople != null) {

                        //get list of attending users
                        final List<User> attendingUsers = mEvent.topAttendingUsers;
                        final int usersCount = attendingUsers.size();

                        //if there is only one user then add the user chip directly else add based on device width
                        if (usersCount == 1) {
                            setUserDetails(llPeople, attendingUsers.get(0));
                        } else {
                            llPeople.post(new Runnable() {
                                @Override
                                public void run() {
                                    Resources resources = getResources();

                                    int peopleViewWidth = llPeople.getMeasuredWidth();
                                    float userChipWidth = resources.getDimension(R.dimen.user_chip_card_width);

                                    //not calculating margins and all
                                    //float cardMargin = resources.getDimension(R.dimen.cards_margin);

                                    float availableWidth = peopleViewWidth - userChipWidth; //assuming icons and people count will take this much space

                                    int numberOfChips = (int) (availableWidth / userChipWidth);

                                    Log.d("number of chips", numberOfChips + "");

                                    //insert user card chips in people's view
                                    int i;
                                    for (i = 0; i < usersCount && i < numberOfChips; i++) {
                                        setUserDetails(llPeople, attendingUsers.get(i));
                                    }

                                    //if some user's are left then add user's count view
                                    if (i < usersCount) {
                                        int usersLeft = usersCount - i;

                                        //insert user's count view
                                        insertUsersCountView(llPeople, usersLeft);
                                    }
                                }
                            });
                        }
                    }
                }

            }
        }
    }

    private void setUserDetails(ViewGroup parent, User user) {
        //set user's details
        View cvUserChip = LayoutInflater.from(EventsActivity.this)
                .inflate(R.layout.layout_user_chip, parent, false);

        //set user's details in the view
        if (cvUserChip != null) {

            //set user's photo
            if (!TextUtils.isEmpty(user.image.thumbUrl)) {
                ImageView ivUserPhoto = (ImageView) cvUserChip.findViewById(R.id.iv_user_photo);
                if (ivUserPhoto != null) {
                    //load thumb image
                    loadImage(ivUserPhoto, user.image.thumbUrl);
                }
            }

            //set user's name
            TextView tvUserName = (TextView) cvUserChip.findViewById(R.id.tv_user_name);
            if (tvUserName != null) {
                tvUserName.setText(user.fullName);
            }

            //set user's email
            TextView tvUserEmail = (TextView) cvUserChip.findViewById(R.id.tv_user_email);
            if (tvUserEmail != null) {
                tvUserEmail.setText(user.email);
            }
        }

        parent.addView(cvUserChip);
    }

    private void insertUsersCountView(ViewGroup parent, int count) {
        //set user's count
        View cvUserCount = LayoutInflater.from(EventsActivity.this)
                .inflate(R.layout.layout_user_count, parent, true);

        if (cvUserCount != null) {

            //set count
            TextView tvUserCount = (TextView) cvUserCount.findViewById(R.id.tv_user_count);
            if (tvUserCount != null) {
                tvUserCount.setText(String.format(Locale.getDefault(), "%d", count));
            }
        }
    }

    private void setAttachedGroup() {
        //set attached group image
        ImageView ivGroupPhoto = (ImageView) findViewById(R.id.iv_group_photo);
        if (ivGroupPhoto != null) {
            loadImage(ivGroupPhoto, mEvent.group.pictureUrl);
        }

        //set attached group name
        TextView tvGroupName = (TextView) findViewById(R.id.tv_group_name);
        if (tvGroupName != null) {
            tvGroupName.setText(mEvent.group.name);
        }
    }

    private void setInvitedBy() {
        //set invited by
        if (!TextUtils.isEmpty(mEvent.invitedBy)) {

            //get invited_by view stub
            ViewStub vsInvitedBy = (ViewStub) findViewById(R.id.vs_invited_by);
            if (vsInvitedBy != null) {

                //inflate invited_by view stub
                TextView tvInvitedBy = (TextView) vsInvitedBy.inflate();

                //set name of inviter
                if (tvInvitedBy != null) {

                    //make inviter's name bold
                    String labelInvitedBy = getString(R.string.invited_by);

                    final SpannableStringBuilder sb = new SpannableStringBuilder(labelInvitedBy + mEvent.invitedBy);
                    final StyleSpan bss = new StyleSpan(Typeface.BOLD); // Span to make text bold
                    sb.setSpan(bss, labelInvitedBy.length(), sb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold

                    tvInvitedBy.setText(sb);
                }
            }
        }
    }

    private void loadImage(ImageView imageView, String url) {
        new ImageLoadingTask(imageView).execute(url);
    }
}
